#!/usr/bin/env python
"""
Created on Wed Nov 09 18:16:25 2021

@author: cernetic
"""
import errno
import os
import sys
from datetime import datetime

import numpy as np
import pygad as pg

VERBOSE = False
pg.environment.verbose = pg.environment.VERBOSE_QUIET

#  ---------------------------------------------------------------------------
#  ------------------------- PATH SETTINGS -----------------------------------
#  ---------------------------------------------------------------------------

# snapshot numbers to track
low, high = 150, 457


SNAPSHOT_PATH_TEMPLATE = (
    "/ptmp/mpa/lnata/snaps_Jeans_limit/snap_dwarf_merger_limjeans_{:03d}.hdf5"
)

FOF_OUTPUT_TEMPLATE = "data/fof/fof_{:03d}.npy"


#  ---------------------------------------------------------------------------
#  ------------------------- FoF parameters-----------------------------------
#  ---------------------------------------------------------------------------
linking_length = "1 pc"
min_N = 100
# ball mask in kpc
# exclude everything inside the 'exclude_inner', set 0 to disable
# exclude everything outside of 'exclude_outer', set to 1e5 or more
exclude_inner = 0
exclude_outer = 10


def make_parent_directory(path: str):
    try:
        os.makedirs(os.path.dirname(path))
    except OSError as exc:  # Python ≥ 2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def make_parent_directory_on_rank_zero(rank: int, path: str):
    if rank == 0:
        make_parent_directory


def run_fof(snum, override=False):
    """
    Run FoF for all snapshots in snum.

    If the tracing file already exists load from that file.

    Otherwize mask the snapshot for only ISM gas and exclude the inner 2kpc
    and run fof on the masked snapshot.
    """
    filename = FOF_OUTPUT_TEMPLATE.format(snum)
    if not override and os.path.isfile(filename):
        print(
            "{} already exists and override flat not set, " "skipping".format(filename)
        )
    else:
        print("processing {}".format(filename))
        s = pg.Snapshot(SNAPSHOT_PATH_TEMPLATE.format(snum))
        s.to_physical_units()

        # -------------------------------------------------
        # ---------- change what to trace here ------------
        # -------------------------------------------------
        # the masked snapshot should be stored in the
        # `snap` variable

        cold_mask = s.gas["temp"] < "300 K"
        cold_ids = s.gas[cold_mask]["ID"]
        star_ids = s.stars["ID"]
        pg.IDMask(np.hstack([cold_ids, star_ids]))
        # warm = (s.gas['temp'] < '2e4 K') & ~cold

        # choose only cold gas or gas and stars (default: gas + stars)
        snap = s[star_ids]
        # snap = cold_mask

        # do not touch anything below
        # -------------------------------------------------
        snap = snap[~pg.BallMask("{} kpc".format(exclude_inner))][
            pg.BallMask("{} kpc".format(exclude_outer))
        ]

        fof, nfof = pg.analysis.find_FoF_groups(
            snap, l=linking_length, min_N=min_N, periodic_boundary=False
        )
        halos = pg.analysis.generate_FoF_catalogue(
            snap,
            FoF=fof,
            max_halos=nfof,
            calc=(
                "mass",
                "Rmax",
                "com",
                "ssc",
                # "Mgas",
                "Mstars",
                "mean_stellar_age",
                # "gas_half_mass_radius_from_com",
                # "gas_half_mass_radius_from_ssc",
                "stars_half_mass_radius_from_com",
                "stars_half_mass_radius_from_ssc",
            ),
        )
        make_parent_directory(filename)
        np.save(filename, halos)
        del halos, s
        print("{} done at {}".format(filename, datetime.now()))


if __name__ == "__main__":
    todo = np.arange(low, high, 1, dtype=int)
    np.random.seed(42)
    np.random.shuffle(todo)
    max_snaps_per_node = 30
    number_of_nodes = 1
    print(f"Working on {number_of_nodes} nodes")

    snap_mask = np.zeros(todo.size, dtype=bool)
    for i, snap in enumerate(todo):
        fname = FOF_OUTPUT_TEMPLATE.format(snap)
        if not os.path.exists(fname):
            snap_mask[i] = True
    print("found a total of {} snapshots".format(snap_mask.sum()))
    todo = todo[snap_mask]

    try:
        print("I am working on snapshot(s) {}".format(todo))
        for snap in todo:
            print(f"currently processing {snap=}")
            run_fof(snap)
    except FileNotFoundError:
        print("No snap for me, exiting")
    print("finished!")
    sys.exit(0)
