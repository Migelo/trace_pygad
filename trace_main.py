#!/usr/bin/env python
"""
Created on Wed Jun 17 23:58:30 2020

@author: cernetic
"""
import gc
import os
import sys

import numpy as np
import pygad as pg
from mpi4py import MPI

from run_fof import (
    FOF_OUTPUT_TEMPLATE,
    SNAPSHOT_PATH_TEMPLATE,
    make_parent_directory_on_rank_zero,
)

VERBOSE = False
pg.environment.verbose = pg.environment.VERBOSE_QUIET


size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()
name = MPI.Get_processor_name()
comm = MPI.COMM_WORLD


class Node:
    def __init__(
        self, progenitors, progenitor_mass, descendants, descendants_mass, mass, props
    ):
        self.progenitors = progenitors
        self.descendants = descendants
        self.progenitor_mass = progenitor_mass
        self.descendants_mass = descendants_mass
        self.mmp = np.argmax(self.progenitor_mass)
        self.mmd = np.argmax(self.descendants_mass)
        self.mass = mass
        self.walked = False
        self.props = props
        if VERBOSE:
            print(
                (
                    "Created a Node with progenitors={}, progenitor mass fraction={} "
                    "mmp={} mass={}"
                ).format(self.progenitors, self.progenitor_mass, self.mmp, self.mass)
            )


class Edge:
    def __init__(self):
        self.times = []
        self.nodes = []
        self.props = []
        # self.coms = []
        # self.Mgass = []
        # self.Mstars = []
        self.lifetime = 0
        self.end = None
        self.com_min = None
        self.com_max = None

    def __str__(self):
        return "lifetime={}\ntimes={}\nnodes={}".format(
            self.lifetime, self.times, self.nodes
        )

    def __repr__(self):
        return "lifetime={}\ntimes={}\nnodes={}".format(
            self.lifetime, self.times, self.nodes
        )

    def finalize(self):
        for prop in self.props[0].keys():
            temp = []
            for i in range(self.lifetime):
                temp.append(self.props[i][prop])
            temp = np.array(temp)
            setattr(self, "{}s".format(prop), temp)
        assert self.end is not None
        for d in range(3):
            self.com_min = np.min(self.coms, axis=0)
            self.com_max = np.max(self.coms, axis=0)


def is_there_a_fof_group_with_these_ids(
    fof_group,
    fof_groups_to_intersect_with,
    threshold=0.5,
    threshold_type="relative",
    snum=None,
    debug=False,
):
    """
    Arguments
    fof_group            (pygad.Halo): Array of IDs to search for
    fof_groups_to_intersect_with (array-like, pg.Halo): Array of halos to be searched
    threshold      (int, double): threshold for the halo considered to be
                                 child/parent
    threshold_type (str): Either 'absolute' or 'relative'. Absolute threshold
                         is the number of particles that match between the
                         old and new halo. For relative the threshold is
                         calculated as 'threshold * halo.IDs.size'

    Return
    -1 if no halo found
    results (array) : Indices of halos from 'halos' which match the
                      'threshold' criteria
    """
    assert threshold_type in ("relative", "absolute"), (
        "threshold type has to" 'be either "relative" or "absolute"'
    )
    if threshold_type == "relative":
        assert threshold <= 1.0, "relative threshold has to be <= 1"

    assert snum is not None

    intersect_mass = []
    IDs = fof_group.IDs
    if VERBOSE:
        print("loading snapshot {}".format(SNAPSHOT_PATH_TEMPLATE.format(snum)))
    s = pg.Snapshot(SNAPSHOT_PATH_TEMPLATE.format(snum))
    s.to_physical_units()

    intersects = np.zeros(len(fof_groups_to_intersect_with), dtype=int)
    intersect_mass = np.zeros(len(fof_groups_to_intersect_with))

    for i, halo in enumerate(fof_groups_to_intersect_with):
        intersect = np.intersect1d(IDs, halo.IDs, assume_unique=False)

        if VERBOSE:
            if intersect.size > 0:
                print(f"intersect count = {intersect.size}")

        if intersect.size >= threshold:
            intersects[i] = 1
            intersect_mask = pg.IDMask(intersect)
            intersect_mass[i] = s[intersect_mask]["mass"].sum()
            if VERBOSE:
                print(f"{intersect_mass[i]=}")

    assert len(intersects) == len(fof_groups_to_intersect_with)

    del s

    gc.collect()

    return intersects, intersect_mass


def generate_node(snapshot: int):
    fname = NODE_OUTPUT_TEMPLATE.format(snapshot)

    if not os.path.exists(fname):
        print(f"doing snapshot {snapshot=}")
    else:
        print(f"skipping snapshot {snapshot=}")
        return

    print(f"loading fof groups for {snapshot=}")
    current_fof_groups = np.load(
        FOF_OUTPUT_TEMPLATE.format(snapshot), allow_pickle=True
    )[1:]
    prev_fof_groups = np.load(
        FOF_OUTPUT_TEMPLATE.format(snapshot - 1), allow_pickle=True
    )[1:]
    next_fof_groups = np.load(
        FOF_OUTPUT_TEMPLATE.format(snapshot + 1), allow_pickle=True
    )[1:]
    print(f"finished loading fof groups for {snapshot=}")

    threshold_type = "absolute"
    threshold = 1

    temp_results = []
    for fof_group_idx, fof_group in enumerate(current_fof_groups):
        progenitors, progenitors_mass = is_there_a_fof_group_with_these_ids(
            fof_group,
            prev_fof_groups,
            threshold=threshold,
            threshold_type=threshold_type,
            snum=snapshot - 1,
            debug=False,
        )
        descendants, descendants_mass = is_there_a_fof_group_with_these_ids(
            fof_group,
            next_fof_groups,
            threshold=threshold,
            threshold_type=threshold_type,
            snum=snapshot + 1,
            debug=False,
        )
        temp_results.append(
            Node(
                progenitors,
                progenitors_mass,
                descendants,
                descendants_mass,
                fof_group.mass,
                fof_group.props,
            )
        )
        print(f"done fof_group {fof_group_idx + 1} / {len(current_fof_groups)}")
    np.save(
        fname,
        temp_results,
        allow_pickle=True,
    )
    del temp_results
    print(f"finished snapshot {snapshot=}")


#  ---------------------------------------------------------------------------
#  ------------------------- USER SETTINGS -----------------------------------
#  ---------------------------------------------------------------------------
threshold = 1
threshold_type = "absolute"  # or "absolute"
"""
with a "relative" threshold a cloud is considered as being "born" from another cloud if
at least "threshold" fraction of its particles come from the parent
while "absolute" the "threshold" is a fixed number, not recommended
"""


# snapshot numbers to track
low, high = 150, 456

NODE_OUTPUT_TEMPLATE = "data/nodes/node_{:03d}.npy"

if __name__ == "__main__":
    todo = np.arange(low + 1, high - 1, 1, dtype=int)
    np.random.seed(42)
    np.random.shuffle(todo)
    max_snaps_per_node = 10
    number_of_nodes = (high - 1 - low - 1) // max_snaps_per_node + 1

    snap_mask = np.zeros(todo.size, dtype=bool)
    for i, snap in enumerate(todo):
        fname = NODE_OUTPUT_TEMPLATE.format(snap)
        if not os.path.exists(fname):
            snap_mask[i] = True
    if rank == 0:
        print("found a total of {} snapshots".format(snap_mask.sum()))
    todo = todo[snap_mask]
    todo_split = np.array_split(todo, number_of_nodes)
    todo_node = todo_split[int(sys.argv[1])]
    todo_ranks = np.array_split(todo_node, max_snaps_per_node)
    todo_rank = todo_ranks[rank]

    make_parent_directory_on_rank_zero(rank, NODE_OUTPUT_TEMPLATE.format(0))

    try:
        sys.stdout.write(
            (
                "Hello, World! I am process {} of {} on {} "
                "working on snapshot(s) {}\n"
            ).format(rank, size, name, todo_rank)
        )
        for snap in todo_rank:
            generate_node(snap)
    except FileNotFoundError:
        sys.stdout.write(
            "Hello, World! I am process %d of %d on %s, no snap for me, exiting\n"
            % (
                rank,
                size,
                name,
            )
        )
    comm.Barrier()
    print("finished!")
    sys.exit(0)
