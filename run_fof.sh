#!/bin/sh
#SBATCH --mail-user cernetic@mpa-garching.mpg.de
#SBATCH --mail-type=ALL
#SBATCH -p p.24h
#SBATCH -J plot
#SBATCH -o run_fof%a.txt
#SBATCH -e run_fof%a.err
#SBATCH -t 24:00:00
#SBATCH --array=0-30


#SBATCH --exclusive
#SBATCH --dependency=singleton
#SBATCH --ntasks 10
#SBATCH --ntasks-per-node=10

export OMP_NUM_THREADS=1
source activate /u/mihac/conda-envs/py2
mpiexec -np "$SLURM_NPROCS" python run_fof.py "$SLURM_ARRAY_TASK_ID"
