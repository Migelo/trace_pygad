# tracing routine for gas blobs, star clusters

1. see `trace.py`, especially the comments in function `run_fof` to set the proper tracing
2. set the appropriate detection threshold and the snapshots to trace starting at line 220 and below
3. limit the number of cores used by `export OMP_NUM_THREADS=1`
4. run the tracing algorithm with `python trace.py`, I recommend you run it on a standalone node (interactive session with `srun -N 1 --cpus-per-task 40 -p p.24h  --pty bash`) or decrease the number of cores used by decresing the number of workers in the pool on line 235
5. open the jupyter notebook for examples of plotting routines
6. fun
